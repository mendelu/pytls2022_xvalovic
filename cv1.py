'''
P1: Vytvorte prazdny nakupni seznam
pridajte 3 polozky
odoberte prvu a poslednu polozku
vlozte polozku na prvu poziciu listu
vypiste list
'''
from tkinter import N

from bleach import clean


nakup = [] # nakup = list()
nakup.append('maslo')
nakup.append('pivo')
nakup.append('rohlik')
# nakup.pop(0) # prvni polozka
# nakup.pop() # posledni
print(nakup[1:])

'''
K nakupnemu zoznamu doplnte 10 rumov
'''
# nakup = nakup + ['rum'] * 10 
nakup += ['rum'] * 10 
print(nakup)


'''
Vygenerjte postupnost cisel delitelnych
5 v intervale <0, 100>
'''
delitelna = list(range(0, 101, 5))
print(delitelna)

'''
Majme struktur list listov ktora 
eviduje studentov a ich vysledok testu
    - vypiste meno studenta s max bodmi
    - vypiste min pocet bodov
'''
studenti = [ 
    ['Znicim Romana', 95], 
    ['Mikulas', 50],
    ['Roman', -5]
]
print(f'Najviac bodov ziskal {studenti[0][0][0]}')
print(f'Najmenej bodov {studenti[-1][1]}')

'''
Majme postupnost cisel s duplicitnymi 
hodnotami. Zbavte sa ich
'''
cisla = [1, 2, 42, 2, 3, 1]
cisla = list(set(cisla))
print(cisla)

'''
Majme kniznice, ktore maju tituly ulozene
v set. 
    - zistite, ktore tituly sa nachadzaju
      v oboch knizniciach
    - zistite set titulov, ktore vzniknu 
      spojenim kniznic
    - tituly, ktore su v mzk ale nie v 
      mahenovej.
'''
mzk = {'Harry Potter', 'Dvacet tisíc mil pod mořem', 'Hoši od Bobří řeky'}
mahenova = {'Harry Potter', 'Babička', 'Macbeth'}

spolecne = mzk.intersection(mahenova)
print(spolecne)

sloucene = mzk.union(mahenova)
print(sloucene)

rozdil = mzk.difference(mahenova)
#rozdil = mahenova.difference(mzk)
print(rozdil)

'''
Naprogramujte jednoduchu kalkulacku,
ktora dokaze scitat a odcitat
Od uzivatela nacitate 1., 2. cislo
a operator (staci +,-)
Vypiste vysledok operacie
Kontrolujte, ci uzivatel zadava skutocne
cislo a povoleny operator. Inak program
ukoncite pomocou prikazu exit
'''
# print('Zadajte 1. cislo')
# a = input()
# print('Zadajte 2. cislo')
# b = input()
# print('Zadajte operator')
# op = input()

# if not a.isnumeric() or not b.isnumeric():
#     print('sorry, toto neni cislo')
#     exit

# if op == '+':
#     vys = int(a) + int(b)
# elif op == '-':
#     vys = int(a) - int(b)
# else:
#     vys = 'neco je spatne'
# print(vys)
'''
    Mame v liste mena clenov timu
    Generujte nazov timu spojenim prvych
    2 pismen clenov timu.
'''
clenove = ['Tomas', 'Petr', 'Nina']
nazev_timu = ''
for clen in clenove:
    nazev_timu += clen[:2]
print(nazev_timu.capitalize())

for idx, _ in enumerate(clenove):
    clenove[idx] += f'-{nazev_timu}'
print(clenove)