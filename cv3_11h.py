a = ['petr', 'jan', 'frantisek']

b = {
    'petr': 1234,
    'jan': 4567
}
b['petr'] # 1234

c = [
    {'jmeno': 'jan', 'cislo': 1234},
    {'jmeno': 'petr', 'cislo': 5678, 
    'deti': ['anicka', 'peta']},
]
c[1]['cislo']

'''
    - Vypiste vsechny sutdenty kteri maji zapsan predmet ALG
    - Vypiste unikatny zoznam vsetkych predmetov
    - Transformujte strukturu tak, ze budeme mat slovnik, kde klucom
      bude kod predmetu a hodnotou zoznam studentov, ktori ho maju
      zapisany
'''
students = [
    {"xlogin": "xvalovic", "jmeno": "Roman", "predmety": ["PYT", "ALG"]},
    {"xlogin": "xhavlik", "jmeno": "Jan", "predmety": ["AI", "ALG"]},
    {"xlogin": "xnadule", "jmeno": "Nada", "predmety": ["WD", "PYT"]},
    {"xlogin": "xmuron", "jmeno": "Mikulas", "predmety": ["TZI", "PTN"]},
    {"xlogin": "xpernes", "jmeno": "Petr", "predmety": ["PYT", "MAT"]}
]

'''
 {
     'ALG': ['xvalovic', 'xpernes'],
     'PYT': [...]
 }
'''

predmety = set()
pred_studenti = {}
for s in students:
    if 'ALG' in s['predmety']:
        print(s['xlogin'])
    for p in s['predmety']:
        predmety.add(p)

for p in predmety:
    pred_studenti[p] = []
    for s in students:
        if p in s['predmety']:
            pred_studenti[p].append(s['xlogin'])
print(pred_studenti)

import kpn
# kpn.play()

'''
priklad: Nacitajte datum narodenia vo formate
YYYY-MM-DD a vypiste vek uzivatela v dnoch
'''
import datetime as dt
bd = '1992-11-05'
today = dt.datetime.today()
bd = dt.datetime.strptime(bd, '%Y-%m-%d')
print(f'Jses starej {today-bd} dni')

'''
    Pomocou modulu time skuste scitat vsetky 
    cisla od 1 do 1000000 a to nasledovne
        - for cyklus
        - metoda sum nad listom #sum([1,2,3])
    Zistite, ktory postup je rychlejsi
'''
import time
suma = 0
hodnoty = list(range(int(10e6)))
T0 = time.time()
for i in hodnoty:
    suma += i
diff = time.time() - T0
print('Scitani for cyklem:', diff)

T0 = time.time()
suma = sum(hodnoty)
diff = time.time() - T0
print('Scitani metodou sum:', diff)


