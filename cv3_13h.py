a = ['petr', 'jan', 'anicka']
a[2]

b = {
    'petr': 1234,
    'jan': 456,
    'anicka': 78945
}
b['anicka']

c = [
        {
            'jmeno': 'anicka', 
            'cislo': 1112, 
            'deti': ['peta', 'honzik']
        }
]

'''
    Ex1:
    - vypiste xlogin studentov ktori maju zapisany 
      predmet ALG
    - vypiste unikatny zoznam predmetov
    - tranformuje strukturu tak, ze vznikne dict kde klucom
      bude kod predmetu a hodnotou list studentov, ktori 
      dany predmet studuju
      {
          'ALG': [],
          'PYT': []
      }
'''
students = [
    {"xlogin": "xvalovic", "jmeno": "Roman", "predmety": ["PYT", "ALG"]},
    {"xlogin": "xhavlik", "jmeno": "Jan", "predmety": ["AI", "ALG"]},
    {"xlogin": "xnadule", "jmeno": "Nada", "predmety": ["WD", "PYT"]},
    {"xlogin": "xmuron", "jmeno": "Mikulas", "predmety": ["TZI", "PTN"]},
    {"xlogin": "xpernes", "jmeno": "Petr", "predmety": ["PYT", "MAT"]}
]

predmety = set()
for clovek in students:
    if 'ALG' in clovek['predmety']:
        print(clovek['xlogin'])
    predmety.update(clovek['predmety'])

predmety_studenti = {}   
for predmet in predmety:
    predmety_studenti[predmet] = []
    for s in students:
        if predmet in s['predmety']:
            predmety_studenti[predmet].append(s['xlogin'])
print(predmety_studenti)

import kamen
# kamen.play()

'''
    Ex: Vytvorte program, ktory prevezme datum narodenia
    vo formate YYYY-MM-DD a vrati vek v dnoch
'''
import datetime as dt 
bd = '1992-11-05'
bd = dt.datetime.strptime(bd, '%Y-%m-%d')
today = dt.datetime.today()
age = today - bd
print(f'Jses mladej {age.days} dni')

'''
    Pomocou modulu time urcite ktory sposob suctu
    hodnot v intervale (1, 10e5) v liste pracuje rychlejsie
    - for cyklus
    - sum # sum([1, 2, 3])
'''
import time
hodnoty = range(int(10e6))
suma = 0
T0 = time.time()
for i in hodnoty:
    suma += i
T1 = time.time()
print(f'for cyklus trval {T1-T0}')

T0 = time.time()
suma = sum(hodnoty)
T1 = time.time()
print(f'sum metoda trvala {T1-T0}')