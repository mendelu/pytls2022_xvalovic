import logging
from cv2 import log
logging.basicConfig(format='%(asctime)s - %(levelname)s : %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.DEBUG)



'''
příklad: Načtěte od uživatele číslo. Použijte pak takto načtené číslo jako dělitele. 
Odchyťte a zpracujte všechny exceptions, které mohou nastat
'''
try:
    # vstup = int(input("Zadej cislo:"))
    vstup = 4
    logging.info(50/vstup)
except ValueError:
    logging.error("Neni cislo")
except ZeroDivisionError:
    logging.error("Nelze delit nulou")


'''
příklad: 
Vytvořte funkci v modulu, který načte od uživatele číslo. Pokud uživatel číslo nezadá, budeme se jej opakovaně ptát dokud číslo nezadá. 
Využijte exception (třeba i rekurzi?)
'''
def nacti_cislo():
    try:
        return int(input("Zadej cislo: "))
    except ValueError:
        return nacti_cislo()
# nacti_cislo()


'''
příklad: Vytvořte modul obsahující funkce, které umožní převod teploty z stupňů Celsia na stupně Fahrenheita a Kelviny a opačně. 
Kontrolujte vstupy a v případě chyby vyvolejte exceptions. Průběžně kontrolujte výsledky mezivýpočtů pomocí assercí. 
'''
f   
import temperatures
temperatures.ceslius_to_kelvin(5)


'''
příklad: Načtěte od uživatele vstup a uložte jej do souboru. Následně soubor načtete a vypište jeho obsah.
'''
# vloz = input('Napis neco: ')

# f = open('data/myFile.txt', 'a')
# f.write(vloz + '\n')
# f.close()

# f = open('data/myFile.txt', 'r')
# for line in f.readlines():
#     # print(line)
#     pass

'''
příklad: 
    Načtěte ze souboru text Hamleta a vypište 20 nejčetnějších slov.  
    Zkuste odstranit stop slova. Zdroj: http://erdani.com/tdpl/hamlet.txt. 
    Dále pak vypište, kolikrát ve v textu vyskytují jména následujících postav poznamka: implementuje bez regexů, jen split a replace)
'''
try:
    f = open('data/hamlet.txt', 'r')
    text = f.readlines()
except FileNotFoundError:
    logging.error('Source file doesnt exist')

characters = ['Hamlet', 'Claudius', 'Polonius', 'Ophelia', 'Gertrude', 'Horatio', 'Laertes', 'Fortinbras', 'The Ghost']
words = []
from stop_words import get_stop_words
from collections import Counter
import re, json

stopwords = get_stop_words('english')
for line in text:
    line = line.strip().lower().split()
    for word in line:
        if word not in stopwords:
            words.append(word)

counter = Counter(words)
print(counter.most_common(10))

with open("output.json", "w") as f:
   json.dump(counter.most_common(20),f)


'''
příklad: Načtěte soubor orders.json (https://akela.mendelu.cz/~xvalovic/vyuka/pyth/orders.json) 
který obsahuje informace o objednávkách v indické restauraci. Zjistěte:
    - Pomocí Counter a metody most_common() vypište 5 nejprodávanejších položek
    - Pomocí defaultdict zjistěte kolik Mango Chutney se prodalo v jednotlivých měsících. (Dataset obsahuje objednávky pouze za rok 2019)
    - Předcházející příklad upravte na funkci, která převezme jídlo ako parametr 
'''

import json 
from collections import Counter

orders = json.load(open('data/orders.json', 'r'))
tmp_list = []
for i in orders:
    tmp_list.extend([i['Item Name']] * i['Quantity'])
meals_qty = Counter(tmp_list)
print(meals_qty.most_common(5))


import datetime as dt
from collections import defaultdict
qty_by_month = defaultdict(int)
for i in orders:
    if i['Item Name'] == 'Mango Chutney':
        date = dt.datetime.strptime(i['Order Date'], '%d/%m/%Y %H:%M')
        qty_by_month[date.month] += i['Quantity']
print(qty_by_month)