class Bytost:
    def __init__(self, sila):
        self.sila = sila
        self.zivoty = 100
 
    def get_utok(self):
        raise NotImplemented
 
    def zautoc(self, nepritel):
        if (nepritel is not self) and self.je_zivy() and nepritel.je_zivy():
            nepritel.uber_zivot(self.get_utok())
 
    def uber_zivot(self, kolik):
        self.zivoty -= kolik
        if self.zivoty < 0:
            self.zivoty = 0
 
    def je_zivy(self):
        return self.zivoty > 0
 
 
class Zbran:
    def __init__(self, typ, bonus_sily):
        self.typ = typ
        self.bonus_sily = bonus_sily
 
    def vypis_info(self):
        print(f"Zbran typu {self.typ} s bonusem {self.bonus_sily}")
 
 
class Lektvar:
    def __init__(self, jmeno, bonus_zivota):
        self.jmeno = jmeno
        self.bonus_zivota = bonus_zivota
 
    def vypis_info(self):
        print(f"Lektvar {self.jmeno} s bonusem {self.bonus_zivota}")
 
 
class Hrdina(Bytost):
    def __init__(self, jmeno, sila):
        self.jmeno = jmeno
        self.zbran = None
        self.lektvary = []
        super().__init__(sila)
 
    def get_utok(self):
        if self.zbran:
            return self.sila + self.zbran.bonus_sily
        return self.sila
 
    def seber_zbran(self, zbran):
        if not self.zbran:
            self.zbran = zbran
 
    def zahod_zbran(self):
        self.zbran = None
 
    def vypis_info(self):
        print(f"Hrdina {self.jmeno} ma {self.zivoty} zivotu a jeho utok je {self.get_utok()}")
        if self.zbran:
            self.zbran.vypis_info()
        for lektvar in self.lektvary:
            lektvar.vypis_info()
 
    def seber_lektvar(self, lektvar):
        self.lektvary.append(lektvar)
 
    def vypij_lektvar(self):
        if self.lektvary:
            lektvar = self.lektvary.pop()
            self.zivoty += lektvar.bonus_zivota
 
 
class Prisera(Bytost):
    def __init__(self, sila, sila_jedu):
        self.sika_jedu = sila_jedu
        super().__init__(sila)
 
    def get_utok(self):
        return self.sila + (self.jedovatost * 10)
 
 
hrdina = Hrdina("Mikulas", 100)
excalibur = Zbran("Excalibur", 50)
prisera = Prisera(10, 5)
pivo = Lektvar("Pivo", 10)
elixir = Lektvar("Elixir", 100)
 
hrdina.seber_zbran(excalibur)
hrdina.seber_lektvar(pivo)
hrdina.seber_lektvar(elixir)
 
hrdina.zautoc(prisera)
hrdina.zautoc(prisera)
prisera.zautoc(hrdina)
 
hrdina.vypij_lektvar()
 
hrdina.vypis_info()