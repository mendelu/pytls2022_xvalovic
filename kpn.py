import random
# from random import choice

def play():
    pc_turn = random.choice(['k', 'n', 'p'])
    my_turn = input('Zadejte vasi volbu (k | n | p)')
    print(pc_turn, my_turn)
    if pc_turn == my_turn:
        print('Remiza')
    elif (pc_turn == 'k' and my_turn == 'n') \
        or (pc_turn == 'n' and my_turn == 'p') \
        or (pc_turn == 'p' and my_turn == 'k'):
        print('Prohral si')
    else:
        print('Vyhral si')

